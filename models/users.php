<?php
define('DefaultRole', 0);//права доступа по умолчанию, обычный пользователь

//TODO: сделать метод delete, в методе edit добавить возможность менять пароль

function user_add($link, $login, $password, $name)
{

    $check = mysqli_query($link, sprintf("SELECT * FROM users WHERE login = '%s'", $login));//Запрос кортежа, где логин совпадает с введёным

    if (mysqli_num_rows($check) == 0) { //если такого кортежа нет

            //Убираем пробелы в начале и конце строки


            $login = trim($login);
            $password = trim($password);
            $name = trim($name);

            $password = md5($password);//хешируем пароль


            //Шаблон запроса
            $q = "INSERT INTO users (login, password, name, role)
    VALUES ('%s', '%s', '%s', '%i')";

            // Формирование запроса
            $query = sprintf($q,
                mysqli_real_escape_string($link, $login),
                mysqli_real_escape_string($link, $password),
                mysqli_real_escape_string($link, $name), DefaultRole);

            $result = mysqli_query($link, $query);

            if (!$result)
                die(mysqli_error($link));
            return true;

    } else {
        return false;
    }


}

function user_auth($link, $login, $password)
{

    if (isset($_POST['login'])) {

        $login = trim($_POST['login']);
        $password = md5($_POST['password']);
        $q = "SELECT login, password, role FROM users WHERE login='%s' and password='%s'"; // где пара логин и пароль совпадает с введенными
        $query = sprintf($q,
            mysqli_real_escape_string($link, $login),
            mysqli_real_escape_string($link, $password));

        $result = mysqli_query($link, $query)
        or die(mysqli_error($link));

        $count = mysqli_num_rows($result);//получаем колличество пользователей
        if ($count == 1) //если пользователь найден
        {
            session_regenerate_id();

            $_SESSION['login'] = $login;
            $query2 = sprintf("SELECT * FROM users WHERE login='%s'", $login);

            $ResultQuery = mysqli_query($link, $query2);

            $result2 = mysqli_fetch_assoc($ResultQuery);

            $role = $result2['role']; // записываем роль из базы данных

            $_SESSION['role'] = $role;
        }

    }
    if ($count == 1)
        return true;
    else return false;
}

function user_logout($link)
{
    $datetime = date('Y/m/d H:i:s');
    $query = sprintf("UPDATE users SET lastacces='%s' WHERE login='%s'", $datetime, $_SESSION['login']);
    $result = mysqli_query($link, $query);
    if (!$result)
        die(mysqli_error($link));

    unset($_SESSION);
    session_destroy();
}

function users_all($link)
{

    $query = "SELECT * FROM users ORDER BY id_users DESC";
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));


    $n = mysqli_num_rows($result);
    $users = array();

    for ($i = 0; $i < $n; $i++) {
        $row = mysqli_fetch_assoc($result);
        $users[] = $row;
    }

    return $users;

}

function users_edit($link, $id_users, $login, $password, $role, $name)
{

    //Убираем пробелы в начале и конце строки
    $login = trim($login);
    $password = md5(trim($password));
    $role = trim($role);
    $name = trim($name);


    $id_users = (int)$id_users;


    //Шаблон запроса
    $q = "UPDATE users SET login='%s', password='%s', role='%d', name='%s' WHERE id_users='%d'";

    // Формирование запроса
    $query = sprintf($q,
        mysqli_real_escape_string($link, $login), mysqli_real_escape_string($link, $password), $role,
        mysqli_real_escape_string($link, $name), $id_users);

    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    return mysqli_affected_rows($link);
}


function users_get($link, $id_users)
{
    $query = sprintf("SELECT * FROM users WHERE id_users='%d'", (int)$id_users);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));
    $users = mysqli_fetch_assoc($result);

    return $users;
}

function users_delete($link, $id_users)
{

    $id_users = (int)$id_users;

    if ($id_users == 0)
        return false;

    $query = sprintf("DELETE FROM users WHERE id_users='%d'", $id_users);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    return mysqli_affected_rows($link);

}

function getIdUser($link, $login)
{
    $login = trim($login);
    $query = sprintf("SELECT id_users FROM users WHERE login='%s'", $login);
    $result = mysqli_query($link, $query);

    if (!$result) die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    $id = $row['id_users'];

    return $id;

    return $users;
}

function getSingleUser($link, $id_users)
{
    $query = sprintf("SELECT * FROM users WHERE id_users='%d'", (int)$id_users);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    $n = mysqli_num_rows($result);
    $users = array();

    for ($i = 0; $i < $n; $i++) {
        $row = mysqli_fetch_assoc($result);
        $users[] = $row;
    }

    return $users;
}

?>