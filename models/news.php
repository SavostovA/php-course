<?php

function news_all($link)
{
    $query = "SELECT * FROM news ORDER BY id_news DESC";
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    //Извлечение из БД

    $n = mysqli_num_rows($result);
    $news = array();

    for ($i = 0; $i < $n; $i++) {
        $row = mysqli_fetch_assoc($result);
        $news[] = $row;
    }

    return $news;
}

function news_get($link, $id_news)
{
    $query = sprintf("SELECT * FROM news WHERE id_news=%d", (int)$id_news);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));
    $singlenews = mysqli_fetch_assoc($result);

    return $singlenews;
}


function news_add($link, $date, $time, $headline, $id_author, $id_heading, $preview, $text)
{

    //Убираем пробелы в начале и конце строки
    $headline = trim($headline);
   $id_author = trim($id_author);
    $id_heading = trim($id_heading);
    $preview = trim($preview);
    $text = trim($text);

    //Шаблон запроса
    $q = "INSERT INTO news (date, time, headline, id_author, id_heading, preview, text)
    VALUES ('%s', '%s', '%s', '%s', '%s', '%s', '%s')";

    // Формирование запроса
    $query = sprintf($q,
        mysqli_real_escape_string($link, $date),
        mysqli_real_escape_string($link, $time),
        mysqli_real_escape_string($link, $headline),
        mysqli_real_escape_string($link, $id_author),
        mysqli_real_escape_string($link, $id_heading),
        mysqli_real_escape_string($link, $preview),
        mysqli_real_escape_string($link, $text));

    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    return true;
}


function news_edit($link, $id_news, $date, $time, $headline, $id_author, $id_heading, $preview, $text)
{
    //Убираем пробелы в начале и конце строки
    $headline = trim($headline);
    $id_author = trim($id_author);
    $id_heading = trim($id_heading);
    $preview = trim($preview);
    $text = trim($text);

    $id_news = (int)$id_news;


    //Шаблон запроса
    $q = "UPDATE news SET date='%s', time='%s', headline='%s', id_author='%s', id_heading='%s', preview='%s', text='%s' WHERE id_news='%d'";

    // Формирование запроса
    $query = sprintf($q,
        mysqli_real_escape_string($link, $date),
        mysqli_real_escape_string($link, $time),
        mysqli_real_escape_string($link, $headline),
        mysqli_real_escape_string($link, $id_author),
        mysqli_real_escape_string($link, $id_heading),
        mysqli_real_escape_string($link, $preview),
        mysqli_real_escape_string($link, $text),
        $id_news);

    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    return mysqli_affected_rows($link);
}


function news_delete($link, $id_news)
{
    $id_news = (int)$id_news;

    if ($id_news == 0)
        return false;

    $query = sprintf("DELETE FROM news WHERE id_news='%d'", $id_news);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    return mysqli_affected_rows($link);
}


function getAuthorNameById($link, $id_author)
{
    $id_author = (int)$id_author;
    $query = sprintf("SELECT name FROM users WHERE id_users='%d'", $id_author);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    $name = $row['name'];

    return $name;

}

function getAuthorIdByLogin($link,$login)
{
    $login=trim($login);
    $query = sprintf("SELECT id_users FROM users WHERE login='%s'", $login);
    $result = mysqli_query($link,$query);

    if(!$result) die(mysqli_error($link));
    $row = mysqli_fetch_assoc($result);
    $id = $row['id_users'];

    return $id;

}

function getHeadingNameById($link, $id_heading)
{
    $id_heading = (int)$id_heading;
    $query = sprintf("SELECT name FROM headings WHERE id_heading='%d'", $id_heading);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    $row = mysqli_fetch_assoc($result);
    $name = $row['name'];

    return $name;
}

function getHeadingIdByName()
{

}
function getAuthorIdByNewsID($link, $id_news){
    $id_news = (int)$id_news;
    $query = sprintf("SELECT id_author FROM news WHERE id_news='%d'", $id_news);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    $row = mysqli_fetch_assoc($result);
    $id_author = $row['id_author'];

    return $id_author;
}

function get_id_headings($link){
    $query = "SELECT id_heading FROM headings ORDER BY id_heading ASC";
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    //Извлечение из БД

    $h = mysqli_num_rows($result);
    $headings = array();

    for ($i = 0; $i < $h; $i++) {
        $row = mysqli_fetch_assoc($result);
        $headings[] = $row;
    }

    return $headings;
}

function getNewsByUser($link, $id_users){
    $q = "SELECT * FROM news WHERE id_author = '%s' ORDER BY id_news DESC ";
    $query = sprintf($q,$id_users);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    //Извлечение из БД

    $n = mysqli_num_rows($result);
    $news = array();

    for ($i = 0; $i < $n; $i++) {
        $row = mysqli_fetch_assoc($result);
        $news[] = $row;
    }

    return $news;
}

function getNewsByHeading($link, $id_heading){
    $q = "SELECT * FROM news WHERE id_heading = '%s' ORDER BY id_news DESC ";
    $query = sprintf($q,$id_heading);
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    //Извлечение из БД

    $n = mysqli_num_rows($result);
    $news = array();

    for ($i = 0; $i < $n; $i++) {
        $row = mysqli_fetch_assoc($result);
        $news[] = $row;
    }

    return $news;
}
function news_allByDate($link)
{
    $query = "SELECT * FROM news ORDER BY date DESC";
    $result = mysqli_query($link, $query);

    if (!$result)
        die(mysqli_error($link));

    //Извлечение из БД

    $n = mysqli_num_rows($result);
    $news = array();

    for ($i = 0; $i < $n; $i++) {
        $row = mysqli_fetch_assoc($result);
        $news[] = $row;
    }

    return $news;
}

?>