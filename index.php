<?php
require_once("Database.php");
require_once("models/news.php");
//TODO: Сделать рефакторинг названий файлов, функций. Упорядочить директории, сделать комментарии в коде
if(!isset($_SESSION))session_start();

$link = db_connect();
$news = news_all($link);
$headings = get_id_headings($link);
$home = pathinfo($_SERVER['REQUEST_URI']);
include("views/news.php");
?>