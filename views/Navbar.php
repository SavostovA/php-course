<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf8">
        <title> Практическая работа №1 </title>
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="\Project\index.php">Home</a>

    <div class="" id="navbarNav">
        <ul class="navbar-nav">


            <?php
            if (isset($_SESSION['role']) && ($_SESSION['role'] == 0 || $_SESSION['role'] == 1)) {
                print "   <li class=\"nav-item\">";
                print "<a class=\"nav-link\" href=\"/Project/Controller/index.php?action=editpanel\">Панель редактирования <span class=\"sr-only\">(current)</span></a> ";
                print " </li>";
            }
            ?>

            <?php
            if (isset($_SESSION['role']) && ($_SESSION['role'] == 0 || $_SESSION['role'] == 1)) {
                print "<li class=\"nav-item\">";
                print " <a class=\"nav-link\" href=\"/Project/Controller/AdminController.php?action=editpanel\">Управление учетной записью</a>";
                print " </li>";
            }
            ?>

            <?php
            if (!isset($_SESSION['login'])) {
                print "<li class=\"nav-item\">";
                print "<a class=\"nav-link\" href=\"/Project/Controller/AuthController.php?action=sign\">Регистрация</a>";
                print "  </li>";
            }
            ?>

            <?php
            if (!isset($_SESSION['login'])) {
                print "<li class=\"nav-item\">";
                print "<a class=\"nav-link\" href=\"/Project/Controller/AuthController.php?action=auth\">Авторизация</a>";
                print "</li>";
            };
            ?>

            <?php
            if (isset($_SESSION['login'])) {
                print "<li class=\"nav-item\">";
                print "<a class=\"nav-link\" href=\"/Project/Controller/AuthController.php?action=logout\">Выйти из системы</a>";
                print "</li>";
            }
            ?>
            <?php if(isset($home)):?>
            <form action="Controller/index.php?action=sortbyheading" method="post" class="form-inline">
            <li class="nav-item dropdown">

                <select name="id_heading"  class="form-control form-control-sm" onchange="this.form.submit()">
                    <option name="id_heading" value="">Рубрика</option>
                    <?php foreach ($headings as $h): ?>
                        <option name="id_heading" value="<?=$h['id_heading']?>">
                            <?php echo getHeadingNameById($link, $h['id_heading']);?></option>
                    <?php endforeach;?>
                </select>
            </li>


            </form>

            <form action='Controller/index.php?action=search' method="POST" class="form-inline">
                <input class="form-control form-control-sm" type="text" placeholder="Search" name="id_news">
                <button class="btn btn-sm btn-outline-success" type="submit">Search</button>
            </form>
            <?php endif;?>
        </ul>
    </div>
</nav>
    </html>
