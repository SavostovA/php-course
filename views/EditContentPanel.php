<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf8">
        <title> Практическая работа №1 </title>
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <?php include("Navbar.php")?>

    <body class="container">

    <br>
    <a href="index.php?action=add">Добавить статью</a>

    <table border="1" class="table table-striped table-hover">
        <tr>
            <th>Дата</th>
            <th>Время</th>
            <th>Заголовок</th>
            <th>Автор</th>
            <th>Рубрика</th>
            <th></th>
            <th></th>
        </tr>

        <?php foreach ($news as $n) : ?>
            <tr>
                <td><?= $n['date'] ?></td>
                <td><?= $n['time'] ?></td>
                <td><?= $n['headline'] ?></td>
                <td><?= getAuthorNameById($link, $n['id_author']) ?></td>
                <td><?= getHeadingNamebyId($link, $n['id_heading']) ?></td>

                <td>
                    <a href="index.php?action=edit&id_news=<?= $n['id_news'] ?>">Редактировать</a>
                </td>

                <td>
                    <a href="index.php?action=delete&id_news=<?= $n['id_news'] ?>">Удалить</a>
                </td>

            </tr>
        <?php endforeach ?>

    </table>


    <div>
        <footer>
            <p>Новостной сайт 2019</p>
        </footer>
    </div>


    </body>
    </html>