<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf8">
        <title> Практическая работа №1 </title>
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
    </head>

    <?php include("Navbar.php")?>

    <body class="container">


    <?php foreach ($news as $n): ?>
        <div class="mt-5">
            <h3>
                <a href="/Project/Controller/index.php?action=singlenews&id_news=<?= $n['id_news'] ?>"><?= $n['headline'] ?></a>
            </h3>
            <em> <?= $n['date'] ?></em>
            <em> <?= $n['time'] ?></em>
            <p class="pads"><?= $n['preview'] ?>...</p>
        </div>
    <?php endforeach ?>
    <div>
        <footer>
            <p>Новостной сайт 2019</p>
        </footer>
    </div>


    </body>
    </html>