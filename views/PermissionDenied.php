<DOCTYPE HTML>
    <link rel="stylesheet" href="../style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
          crossorigin="anonymous">
    <html>
    <div class="alert alert-danger container mt5"  role="alert">
        <h4 class="alert-heading">Permission denied</h4>
        <p>You do not have access to this page, please log in.</p>
        <hr>
        <p class="mb-0">
            <a href="../index.php">Return to the main page</a>
        </p>
    </div>
    </html>
