<html>
<head>
    <meta charset="utf8">
    <title> Практическая работа №1 </title>
    <link rel = "stylesheet" href = "style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>

<?php include("Navbar.php")?>

<body class="container">


<div class="article">

    <h3><?=$singlenews['headline']?></h3>

    <em> Дата: <?=$singlenews['date']?></em>
    <em> Время: <?=$singlenews['time']?></em>

    <div>
        <p class="text"><?=$singlenews['text']?>...</p>
    </div>


</div>

<div>
    <footer>
        <p>Новостной сайт 2019</p>
    </footer>
</div>
</body>
</html>