<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf8">
        <title> Практическая работа №1 </title>
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <?php include("Navbar.php")?>

    <body class="container">


    <div class="container">
        <form method="post" action="index.php?action=add">

            <label class="row">
                Дата
                <input type="date" name="date" value=""
                       class="form-control" autofocus required>
            </label>

            <label class="row">
                Время
                <input type="time" name="time" value=""
                       class="form-control" autofocus required>
            </label>

            <label class="row">
                Заголовок
                <input type="text" name="headline" value=""
                       class="form-control" required>
            </label>

            <label class="row">
                Рубрика
                <select name="id_heading" class="form-control">
                    <?php foreach ($headings as $h): ?>
                <option value="<?=$h['id_heading']?>"><?php echo getHeadingNameById($link, $h['id_heading']);?></option>
                    <?php endforeach;?>
                </select>
            </label>

            <label class="row">
                Анонс
                <input type="text" name="preview" value=""
                       class="form-control" required>
            </label>

            <div class="form-group row">
                <label for="exampleFormControlTextarea1">Содержание</label>
                <textarea class="form-control" name="text" id="exampleFormControlTextarea1" rows="12"></textarea>
            </div>

            <button type="submit" class="btn btn-primary btn-lg">Сохранить</button>
        </form>

    </div>

    <div>
        <footer>
            <p>Новостной сайт 2019</p>
        </footer>
    </div>

    </body>
    </html>