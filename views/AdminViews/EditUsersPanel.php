<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf8">
        <title> Практическая работа №1 </title>
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <?php include("C:\\xampp\htdocs\Project\\views\Navbar.php")?>

    <body class="container">
    <br>
    <?php if ($_SESSION['role'] == 1): ?><a href="../Controller/AdminController.php?action=add">Добавить пользователя</a><?php endif; ?>


    <table border="1" class="table table-striped table-hover">
        <tr>
            <?php if ($_SESSION['role'] == 1): ?><th>ID Пользователя</th><?php endif; ?>
            <?php if ($_SESSION['role'] == 1): ?> <th>Логин</th><?php endif; ?>
            <th>Пароль</th>
            <?php if ($_SESSION['role'] == 1): ?>   <th>Права доступа</th><?php endif; ?>
            <th>Имя пользователя</th>
            <th>Время выхода</th>
            <th></th>
            <?php if ($_SESSION['role'] == 1): ?><th></th><?php endif; ?>

        </tr>

        <?php foreach ($users as $u) : ?>
            <tr>

                <?php if ($_SESSION['role'] == 1): ?><td><?= $u['id_users'] ?></td><?php endif; ?>
                <?php if ($_SESSION['role'] == 1): ?><td><?= $u['login'] ?></td><?php endif; ?>
                <td>********</td>

                <?php if ($_SESSION['role'] == 1): ?>
                    <td><?php if ($u['role'] == 0 ) echo "user"; else echo"root"?></td>
                <?php endif; ?>

                <td><?= $u['name']?></td>
                <td><?= $u['lastacces']?></td>

                <td>
                    <a href="../Controller/AdminController.php?action=edit&id_users=<?= $u['id_users'] ?>">Редактировать</a>
                </td>

                <?php if ($_SESSION['role'] == 1): ?>
                    <td>
                    <a href="../Controller/AdminController.php?action=delete&id_users=<?= $u['id_users'] ?>">Удалить</a>
                    </td>
                <?php endif; ?>


            </tr>
        <?php endforeach ?>

    </table>


    <div>
        <footer>
            <p>Новостной сайт 2019</p>
        </footer>
    </div>


    </body>
    </html>
