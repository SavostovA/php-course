<DOCTYPE html>
    <html>
    <head>
        <meta charset="utf8">
        <title> Практическая работа №1 </title>
        <link rel="stylesheet" href="../style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
              crossorigin="anonymous">
    </head>

    <?php include("C:\\xampp\htdocs\Project\\views\Navbar.php") ?>

    <body class="container">


    <div class="container">
        <form method="post"
              action="AdminController.php?action=<?= $_GET['action'] ?>&id_users=<?= $_GET['id_users'] ?>">

            <?php if ($_SESSION['role'] == 1): ?>
                <label class="row">
                    Логин
                    <input type="text" name="login" value="<?= $users['login'] ?>"
                           class="form-control" required autofocus>
                </label>
            <?php endif; ?>

            <label class="row">
                Пароль
                <input type="password" name="password" value="********"
                       class="form-control" required>
            </label>

            <?php if ($_SESSION['role'] == 1): ?>
            <label class="row">
                Права доступа
                <select name="role" class="form-control">
                    <option value="0" <?php if ($users['role'] == 0) echo "selected" ?>> User</option>
                    <option value="1" <?php if ($users['role'] == 1) echo "selected" ?>> Root</option>
                </select>
                <?php endif; ?>

            </label>
            <label class="row">
                Имя пользователя
                <input type="text" name="name" value="<?= $users['name'] ?>
" class="form-control" required>
            </label>

            <br>

            <label class="row">
                <button type="submit" class="btn btn-primary btn-block ">Изменить</button>
            </label>

        </form>
    </div>

    <div>
        <footer>
            <p>Новостной сайт 2019</p>
        </footer>
    </div>

    </body>
    </html>