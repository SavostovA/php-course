<?php
require_once("../Database.php");
require_once("../models/users.php");

$link = db_connect();
session_start();

if (isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "";

if ($action == "sign" && !isset($_SESSION['role'])) {
    if (!empty($_POST)) {
        if ($_POST['password'] == $_POST['password2'] && $_POST['login'] != "" && (user_add($link, $_POST['login'], $_POST['password'], $_POST['name']) == true)) {
            header("Location: ../index.php");
        } else header("Location: AuthController.php?action=sign&err=true");
    }
    include("../views/AuthViews/signup.php");

} else

    if ($action == "auth" && !isset($_SESSION['role'])) {
        if (!empty($_POST)) {

            user_auth($link, $_POST['login'], $_POST['password']);
            header("Location: ../index.php");
        }

        include("../views/AuthViews/Auth.php");

    } else
        if ($action == "logout" && isset($_SESSION['role']) && ($_SESSION['role'] == 0 || $_SESSION['role'] == 1)) {
            user_logout($link);
            header("Location: ../index.php");
        } else
            include("../index.php")
?>