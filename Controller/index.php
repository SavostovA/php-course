<?php
require_once("../Database.php");
require_once("../models/news.php");

session_start();

$link = db_connect();


if (isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "";

if ($action == "editpanel" && isset($_SESSION['role']) && ($_SESSION['role'] == 0 || $_SESSION['role'] == 1)) {
    if ($_SESSION['role'] == 0) {
        $news = getNewsByUser($link, getAuthorIdByLogin($link, $_SESSION['login']));
    } else
        $news = news_all($link);
    include("../views/EditContentPanel.php");
} else

    if ($action == "singlenews") {
        if (!empty($_GET['id_news'])) {
            $singlenews = news_get($link, $_GET['id_news']);
            include("../views/singlenews.php");
        }

    } else

        if ($action == "add" && isset($_SESSION['role']) && ($_SESSION['role'] == 0 || $_SESSION['role'] == 1)) {
            if (!empty($_POST)) {
                news_add($link, $_POST['date'], $_POST['time'],
                    $_POST['headline'], getAuthorIdByLogin($link, $_SESSION['login']), $_POST['id_heading'],
                    $_POST['preview'], $_POST['text']);
                header("Location: index.php?action=editpanel"); //отправка http заголовка
            }
            $headings = get_id_headings($link);
            include("../views/addnews.php");

        } else

            if ($action == "edit" && isset($_SESSION['role']) &&
                (getAuthorIdByLogin($link, $_SESSION['login']) == (getAuthorIdByNewsID($link, $_GET['id_news']))
                    || $_SESSION['role'] == 1)) { // Блок редактирования

                if (!isset($_GET['id_news']))
                    header("Location: index.php?action=editpanel");
                $headings = get_id_headings($link);
                $id_news = (int)$_GET['id_news'];  /////////////////////////////////////

                if (!empty($_POST) && $id_news > 0) {
                    news_edit($link, $id_news, $_POST['date'], $_POST['time'],
                        $_POST['headline'], getAuthorIdByLogin($link, $_SESSION['login']),
                        $_POST['id_heading'], $_POST['preview'], $_POST['text']);
                    header("Location: index.php?action=editpanel");
                }
                $news = news_get($link, $id_news);
                include("../views/editnews.php"); ////////////////////

            } else

                if ($action == "delete" && isset($_SESSION['role']) &&
                    (getAuthorIdByLogin($link, $_SESSION['login']) == (getAuthorIdByNewsID($link, $_GET['id_news']))
                        || $_SESSION['role'] == 1)) {
                    $id_news = (int)$_GET['id_news'];
                    news_delete($link, $id_news);
                    header("Location: index.php?action=editpanel"); //отправка http заголовка

                } else

                    if ($action == "") {
                    $action="editpanel";
                    header("Location: index.php?action=editpanel");
                } else

                    if($action == "search") {
                        if (!empty($_POST['id_news'])) {
                            $singlenews = news_get($link, $_POST['id_news']);
                            include("../views/singlenews.php");}


                        } else

                            if ($action == "sortbyheading") {
                                if (!empty($_POST['id_heading'])) {

                                    $news = getNewsByHeading($link, $_POST['id_heading']);
                                    include("../views/news.php");
                                }

                            } else {
                                include("../views/PermissionDenied.php");

                    }
?>