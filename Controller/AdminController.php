<?php
require_once("../Database.php");
require_once("../models/users.php");

//TODO: Сделать возможность менять данные учетной записи, пользователю, который является владельцев аккаутна
session_start();

$link = db_connect();


if (isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "";

if ($action == "editpanel" && isset($_SESSION['role']) && ($_SESSION['role'] == 0 || $_SESSION['role'] == 1 )) {
  if ($_SESSION['role'] == 0){ $users = getSingleUser($link, getIdUser($link,$_SESSION['login']));} else //костыль с getSingleUser вместо users_get, так как edit_usersй использует массив строк, а не assoc
    $users = users_all($link);
    include("../views/AdminViews/EditUsersPanel.php");
} else

    if ($action == "add" && isset($_SESSION['role']) && $_SESSION['role'] == 1) {
        if (!empty($_POST)) {

            user_add($link, $_POST['login'], $_POST['password'], $_POST['name']);
            header("Location: AdminController.php?action=editpanel"); //отправка http заголовка
        }
        include("../views/AdminViews/adduser.php");
    } else

    if ($action == "edit" && isset($_SESSION['role']) && ($_SESSION['role'] == 0 || $_SESSION['role'] == 1 )) { // Блок редактирования

        if (!isset($_GET['id_users']))
            header("Location: AdminController.php?action=editpanel");


        $id_users = (int)$_GET['id_users'];  /////////////////////////////////////

        if (!empty($_POST) && $id_users > 0) {
            if($_SESSION['role']==0) {$login = $_SESSION['login'];
                $role = $_SESSION['role'];}
            else {$login=$_POST['login']; $role = $_POST['role'];}
            users_edit($link, $id_users, $login, $_POST['password'], $role,
                $_POST['name']);
            header("Location: AdminController.php?action=editpanel");
        }
        $users = users_get($link, $id_users);
        include("../views/AdminViews/edituser.php"); ////////////////////
        } else

            if ($action == "delete" && isset($_SESSION['role']) && $_SESSION['role'] == 1) {
            $id_users = (int)$_GET['id_users'];
            if($id_users == 1){
                header("Location: ../views/easter.php");

            }else{
            users_delete($link, $id_users);
            header("Location: AdminController.php?action=editpanel");} //отправка http заголовка
        }else
            {
    include("../views/PermissionDenied.php");
}



?>